// React Imports
import React from 'react';
import ReactDOM from 'react-dom';


// CSS Imports
import './index.css';


// Component Imports
import App from './App';


ReactDOM.render(
	<App />,
	document.getElementById('root')
);