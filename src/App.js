// React Imports
import React from 'react';


// CSS Imports


// Components Imports


const App = () => {
	/**
	 * Component Function to render the entire App.
	*/
	
	return (
		<React.Fragment>
			<h1>APP</h1>
		</React.Fragment>
	);
};


export default App;